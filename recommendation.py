import pandas as pd
import numpy as np
import tensorflow as tf
import utils
import pickle

PAD = 0
EOS = 1


def prepare_model(sess, vocab_size):
    
    input_embedding_size = vocab_size * 2
    encoder_hidden_units = vocab_size * 2
    decoder_hidden_units = encoder_hidden_units
    
    
    encoder_inputs = tf.placeholder(shape=(None, None), dtype=tf.int32, name='encoder_inputs')
    decoder_targets = tf.placeholder(shape=(None, None), dtype=tf.int32, name='decoder_targets')
    decoder_inputs = tf.placeholder(shape=(None, None), dtype=tf.int32, name='decoder_inputs')
    embeddings = tf.Variable(tf.random_uniform([vocab_size, input_embedding_size], -1.0, 1.0), dtype=tf.float32)
    encoder_inputs_embedded = tf.nn.embedding_lookup(embeddings, encoder_inputs)
    decoder_inputs_embedded = tf.nn.embedding_lookup(embeddings, decoder_inputs)
    encoder_cell = tf.contrib.rnn.LSTMCell(encoder_hidden_units)

    encoder_outputs, encoder_final_state = tf.nn.dynamic_rnn(
        encoder_cell, encoder_inputs_embedded,
        dtype=tf.float32, time_major=True,
    )

    del encoder_outputs

    decoder_cell = tf.contrib.rnn.LSTMCell(decoder_hidden_units)

    decoder_outputs, decoder_final_state = tf.nn.dynamic_rnn(
        decoder_cell, decoder_inputs_embedded,
        initial_state=encoder_final_state,
        dtype=tf.float32, time_major=True, scope="plain_decoder",
    )

    decoder_logits = tf.contrib.layers.linear(decoder_outputs, vocab_size)
    decoder_prediction = tf.nn.softmax(decoder_logits)

    stepwise_cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
        labels=tf.one_hot(decoder_targets, depth=vocab_size, dtype=tf.float32),
        logits=decoder_logits,
    )

    loss = tf.reduce_mean(stepwise_cross_entropy)
    train_op = tf.train.AdamOptimizer().minimize(loss)
    
    sess.run(tf.global_variables_initializer())
    
    return encoder_inputs, decoder_targets, decoder_inputs, decoder_prediction, loss, train_op



def train_recommendation(df, client, batch_size, column_map):
    
    tf.reset_default_graph()    
    sess = tf.InteractiveSession()

    df = df[list(column_map.values())].sort_values(column_map['timestamp'])
    df['user_coded'] = df[column_map['user']].astype('category').cat.codes
    df['product_coded'] = df[column_map['product']].astype('category').cat.codes + 2
    
    vocab_size = int(df['product_coded'].max()) + 1
    product_codes = dict(df[['product_coded', column_map['product']]].values)
    product_codes[1] = 'EOS'

    product_map = {'vocab_size': vocab_size, 'product_codes': product_codes}
    
    df = df[['user_coded','product_coded']].drop_duplicates(subset=None, keep='first', inplace=False)
    df = df.groupby('user_coded')['product_coded'].apply(list).reset_index()
    
    result_df = pd.DataFrame()
    result_df['llist'] = sum([utils.expand_list(l) for l in df['product_coded']],[])
    result_df['rlist'] = [[l[-1]] for l in result_df['llist']]
    result_df['llist'] = [l[:-1] for l in result_df['llist']]
    result_df = result_df[result_df.astype(str)['llist'] != '[]']
    
    row_count = result_df['rlist'].values.shape[0]
    number_of_batches = int(np.ceil(row_count/batch_size))
    
    
    llist_batches = utils.chunks(result_df['llist'].values, batch_size)
    rlist_batches = utils.chunks(result_df['rlist'].values, batch_size)
    
    del df
    del result_df
    
    input_embedding_size = vocab_size * 2
    
    encoder_hidden_units = vocab_size * 2
    decoder_hidden_units = encoder_hidden_units
    
    encoder_inputs, decoder_targets, decoder_inputs, decoder_prediction, loss, train_op = prepare_model(sess,vocab_size)
    
    def next_feed():
            encoder_batch = next(llist_batches)
            decoder_batch = next(rlist_batches)
        
            encoder_inputs_, _ = utils.batch(encoder_batch)
            decoder_targets_, _ = utils.batch(
                [(sequence) + [EOS] for sequence in decoder_batch]
            )
            decoder_inputs_, _ = utils.batch(
                [[EOS] + (sequence) for sequence in decoder_batch]
            )
            return {
                encoder_inputs: encoder_inputs_,
                decoder_inputs: decoder_inputs_,
                decoder_targets: decoder_targets_,
            }
        
    loss_track = []
    
    
    try:
        for batch in range(number_of_batches):
            fd = next_feed()
            _, l = sess.run([train_op, loss], fd)
            loss_track.append(l)
    
            print('batch {}'.format(batch))
            print('  minibatch loss: {}'.format(sess.run(loss, fd)))
                
    except KeyboardInterrupt:
        print('training interrupted')
    
    saver = tf.train.Saver()
    saver.save(sess, client +"/model.ckpt")
    
    
    pickle.dump( product_map , open(client + "/product_map.p", "wb" ) )


def predict_recommendation(client, product_map, data, recom_length):
    
    tf.reset_default_graph()    
    sess = tf.InteractiveSession()
    encoder_inputs, decoder_targets, decoder_inputs, decoder_prediction, loss, train_op = prepare_model(sess,product_map['vocab_size'])
    saver = tf.train.Saver()
    saver.restore(sess, client +"/model.ckpt")

    
    def prediction_feed(data):
    
        encoder_inputs_, _ = utils.batch(data)
        
        decoder_targets_, _ = utils.batch(
            [[EOS]]
        )
        decoder_inputs_, _ = utils.batch(
            [[EOS]]
        )
        return {
            encoder_inputs: encoder_inputs_,
            decoder_inputs: decoder_inputs_,
            decoder_targets: decoder_targets_,
        }
     
    fd = prediction_feed([data])
    predict_probabilities = sess.run(decoder_prediction, fd)
    prediction_list = predict_probabilities[0][0].argsort()[-recom_length:][::-1]
    
    return prediction_list

